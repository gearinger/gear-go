package TestFunc

import "fmt"

// 函数
func Method1(a, b int) {
	fmt.Println(a + b)
}

// 函数作为参数
func Method2(a func(a, b int), c int, d int) {
	fmt.Println("a + b = ")
	a(c, d)
}
