package TestVar

import "fmt"

// 变量
func Vari() {
	// var声明类型，后赋值。int默认值0
	var a int
	a = 1
	// var直接赋值
	var b = 1
	// 语法糖 :=
	c := 1
	fmt.Println(a, b, c)

	// 多变量同时赋值
	var t1, t2, t3 = 1, 2, 3
	fmt.Println(t1, t2, t3)
	// 多变量同时赋值
	var (
		d string
		e int
	)
	fmt.Println(d, e)
}
