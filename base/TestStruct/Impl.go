package TestStruct

import "fmt"

// Method1 实现类	结构体和接口的实现必须在同一包
func (u User) Method1() {
	fmt.Println("~~~~method1~~~~")
}
func (u User) Method2() {
	fmt.Println("~~~~method2~~~~")
}
