package TestStruct

// User 结构体	结构体和接口的实现必须在同一包
type User struct {
	Name   string
	Age    int
	Weight float32
}
