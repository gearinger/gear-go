package main

import (
	"fmt"
	"go.learn/TestFunc"
	"go.learn/TestInterface"
	"go.learn/TestStruct"
	main2 "go.learn/TestVar"
	"reflect"
)

func main() {
	fmt.Println("~~~~~~~~~~run~~~~~~~~~")

	// 变量
	main2.Vari()

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// 函数
	TestFunc.Method2(TestFunc.Method1, 1, 2)

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// 结构体
	u := TestStruct.User{Name: "Tom", Age: 1, Weight: 55.0}
	u2 := new(TestStruct.User) // 返回结构体的指针
	u3 := &TestStruct.User{}   // 返回结构体的指针
	fmt.Println("结构体:")
	fmt.Println(u, u2, u3)

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// 接口
	// go语言中的接口并不直接限制结构体。而是，可以将满足接口的结构体，赋给对应接口类型的变量
	var i TestInterface.Interface1 = TestStruct.User{}
	i.Method1()
	fmt.Println(i, reflect.TypeOf(i))
}
