package main

import (
	"go.learn/gear-gin/Databases"
	"go.learn/gear-gin/Router"
)

func main() {
	defer Databases.DB.Close()
	Router.InitRouter()
}
